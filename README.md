# soal-shift-sisop-modul-2-D12-2022

## Anggota Kelompok
- Marcellino Mahesa Janitra 5025201105
- Afiq Akram 5025201270
- Samuel Berkat Hulu 5025201055

# Soal. 1
## apa yang diminta dari soal?
- implementasikan Thread dalam pengerjaan soal ini
1. unzip file zip
2. decode file memakai base64 decoder
3. masukan dan gabungkan hasil decode ke file txt
4. zip file txt menjadi hasil.zip
5. unzip lalu buat file no.txt yang berisi "No"
6. zip kembali menggunakan password 'mihinomenest[Nama user]'

## Penyelesaian
- Pembuatan Thread
```c
    pthread_t id=pthread_self();
```
- Penggunaan Thread
```c
pthread_t id=pthread_self();
    if(pthread_equal(id,tid1[0]))
	{
        pid_t child;
		child = fork();
		if (child==0) {
		    execv("/usr/bin/clear", argv);
	    }
	}
	else if(pthread_equal(id,tid1[1]))
	{
        pid_t child;
        child = fork();
        if (child == 0) {
            execv("/usr/bin/unzip", argv1);
        } else {
            wait(0);
        }
	}
	else if(pthread_equal(id,tid1[2]))
	{
        pid_t child;
        child = fork();
        if (child == 0) {
            execv("/usr/bin/unzip", argv2);
        } else {
            wait(0);
        }
	}
```
- Decoder b64
diambil dari: https://stackoverflow.com/questions/342409/how-do-i-base64-encode-decode-in-c

- Pembuatan File
menggunakan Struct FILE dan DIR dari c

- membuka file temp:
```c
DIR *dir;
struct dirent *ep;
dir = opendir("quote");
```

- memasukan hasil decode ke file
```c
FILE * f = fopen ("quote.txt", "a");
fprintf(f, "%s", base64_decode(buffer, length, &length));
fprintf(f, "\n");
fclose (f);
```
- membuat file no.txt
```c
FILE *f = fopen("no.txt", "wr");
fprintf(f, "No");
fclose(f);
```

- zipping file dan mmeberi password
menggunakan fork (metode multi process)
```c
if (fork() == 0) {
    char *arg[] = {"zip", "-r", "-P", buf, "hasil.zip", "hasil/", "no.txt", NULL};
    execv("/usr/bin/zip", arg);   
} else {
    while ((wait(&status)) > 0);
}
```
