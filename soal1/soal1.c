#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdint.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<dirent.h>
#include <pwd.h>

// b64 decodernya gw curi dari https://stackoverflow.com/questions/342409/how-do-i-base64-encode-decode-in-c
static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};

void build_decoding_table() {

    decoding_table = malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}


void base64_cleanup() {
    free(decoding_table);
}

unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length) {

    if (decoding_table == NULL) build_decoding_table();

    if (input_length % 4 != 0) return NULL;

    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;

    for (int i = 0, j = 0; i < input_length;) {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}

pthread_t tid1[3], tid2[3], tid3[3], tid4[4];

// function to unzip 2 files using thread
void *unzip(void *arg)
{
    char *argv[] = {"clear", NULL};
    char *argv1[] = {"unzip", "music.zip", "-d", "music", NULL};
    char *argv2[] = {"unzip", "quote.zip", "-d", "quote", NULL};
    pthread_t id=pthread_self();
    if(pthread_equal(id,tid1[0]))
	{
        pid_t child;
		child = fork();
		if (child==0) {
		    execv("/usr/bin/clear", argv);
	    }
	}
	else if(pthread_equal(id,tid1[1]))
	{
        pid_t child;
        child = fork();
        if (child == 0) {
            execv("/usr/bin/unzip", argv1);
        } else {
            wait(0);
        }
	}
	else if(pthread_equal(id,tid1[2]))
	{
        pid_t child;
        child = fork();
        if (child == 0) {
            execv("/usr/bin/unzip", argv2);
        } else {
            wait(0);
        }
	}

	return NULL;
}

void *decode(void *arg)
{
    char *argv[] = {"clear", NULL};
    pthread_t id=pthread_self();
    if(pthread_equal(id,tid2[0]))
	{
        pid_t child;
		child = fork();
		if (child==0) {
		    execv("/usr/bin/clear", argv);
	    } else {
            wait(0);
        }
	}
	else if(pthread_equal(id,tid2[1])) 
	{
        DIR *dir;
        struct dirent *ep;
        dir = opendir("music");
        if (dir != NULL) {
            while ((ep = readdir (dir))) {
                if ( !strcmp(ep->d_name, ".") || !strcmp(ep->d_name, "..") ) {}
                else {
                    char filename [264];
                    char * buffer = 0;
                    long length;
                    sprintf(filename, "./music/%s", ep->d_name);
                    FILE * f = fopen (filename, "rb");
                    
                    if (f)
                    {
                        fseek (f, 0, SEEK_END);
                        length = ftell (f);
                        fseek (f, 0, SEEK_SET);
                        buffer = malloc (length);
                    if (buffer)
                    {
                        fread (buffer, 1, length, f);
                    }
                        fclose (f);
                    }

                    if (buffer)
                    {
                        FILE * f = fopen ("music.txt", "a");
                        fprintf(f, "%s", base64_decode(buffer, length, &length));
                        fprintf(f, "\n");
                        fclose (f);
                    }
                }
            }
            (void) closedir (dir);
        } else perror ("Couldn't open the directory");
	} 
    else if(pthread_equal(id,tid2[2])) 
	{
        DIR *dir;
        struct dirent *ep;
        dir = opendir("quote");
        if (dir != NULL) {
            while ((ep = readdir (dir))) {
                if ( !strcmp(ep->d_name, ".") || !strcmp(ep->d_name, "..") ) {}
                else {
                    char filename [264];
                    char * buffer = 0;
                    long length;
                    sprintf(filename, "./quote/%s", ep->d_name);
                    FILE * f = fopen (filename, "rb");
                    
                    if (f)
                    {
                        fseek (f, 0, SEEK_END);
                        length = ftell (f);
                        fseek (f, 0, SEEK_SET);
                        buffer = malloc (length);
                        if (buffer)
                        {
                            fread (buffer, 1, length, f);
                        }
                        fclose (f);
                    }

                    if (buffer)
                    {
                        FILE * f = fopen ("quote.txt", "a");
                        fprintf(f, "%s", base64_decode(buffer, length, &length));
                        fprintf(f, "\n");
                        fclose (f);
                    }
                }
            }
            (void) closedir (dir);
        } else perror ("Couldn't open the directory");
	}
    return NULL;
}

void *useless_thing(void *arg)
{
    pthread_t id=pthread_self();
    // use thread to delete directory hasil, music, quote, and file no.txt
    if(pthread_equal(id,tid4[0]))
    {
        pid_t child;
        char *argv[] = {"rm", "-rf", "hasil", NULL};
        child = fork();
        if (child==0) {
            execv("/usr/bin/rm", argv);
        } else {
            wait(0);
        }
    }
    else if(pthread_equal(id,tid4[1])) 
    {
        pid_t child;
        char *argv[] = {"rm", "-rf", "music", NULL};
        child = fork();
        if (child==0) {
            execv("/usr/bin/rm", argv);
        } else {
            wait(0);
        }
    } 
    else if(pthread_equal(id,tid4[2])) 
    {
        pid_t child;
        char *argv[] = {"rm", "-rf", "quote", NULL};
        child = fork();
        if (child==0) {
            execv("/usr/bin/rm", argv);
        } else {
            wait(0);
        }
    }
    else if(pthread_equal(id,tid4[3])) 
    {
        pid_t child;
        char *argv[] = {"rm", "-rf", "no.txt", NULL};
        child = fork();
        if (child==0) {
            execv("/usr/bin/rm", argv);
        } else {
            wait(0);
        }
    }
    return NULL;
}

void *move_file(void *arg) 
{
    char *argv[] = {"mkdir", "-p", "hasil", NULL};
    pthread_t id=pthread_self();
    if(pthread_equal(id,tid3[0]))
    {
        pid_t child;
        child = fork();
        if (child==0) {
            execv("/usr/bin/mkdir", argv);
        } else {
            wait(0);
        }
    }
    else if(pthread_equal(id,tid3[1])) 
    {
        pid_t child;
        char *argv1[] = {"mv", "music.txt", "hasil", NULL};
        child = fork();
        if (child==0) {
            execv("/usr/bin/mv", argv1);
        } else {
            wait(0);
        }
    } 
    else if(pthread_equal(id,tid3[2])) 
    {
        pid_t child;
        char *argv2[] = {"mv", "quote.txt", "hasil", NULL};
        child = fork();
        if (child==0) {
            execv("/usr/bin/mv", argv2);
        } else {
            wait(0);
        }
    }
    return NULL;
}


int main() {
    int i;
    for(i=0;i<3;i++)
    {
        int err = pthread_create(&(tid1[i]), NULL, &unzip, NULL);
        if (err != 0)
            printf("\ncan't create thread :[%s]", strerror(err));
    }
    for(i=0;i<3;i++)
    {
        pthread_join(tid1[i], NULL);
    }

    for(i=0;i<3;i++)
    {
        int err = pthread_create(&(tid2[i]), NULL, &decode, NULL);
        if (err != 0)
            printf("\ncan't create thread :[%s]", strerror(err));
    }
    for(i=0;i<3;i++)
    {
        pthread_join(tid2[i], NULL);
    }

    for(i=0;i<3;i++)
    {
        int err = pthread_create(&tid3[i], NULL, &move_file, NULL);
        if (err != 0)
            printf("\ncan't create thread :[%s]", strerror(err));
    }
    for(i=0;i<3;i++)
    {
        pthread_join(tid3[i], NULL);
    }

    struct passwd *pw = getpwuid(getuid());
    char buf[100];
    int status;
    sprintf(buf, "mihinomenest%s", pw->pw_name);

    // unecessary steps, ngapain di zip trus di unzip lagi

    // if (fork() == 0) {
    //     char *arg[] = {"zip", "-r", "-P", buf, "hasil.zip", "hasil/", NULL};
    //     execv("/usr/bin/zip", arg);
    // } else {
    //     while ((wait(&status)) > 0);
    // }

    // if (fork() == 0) {
    //     char *arg[] = {"unzip", "-P", buf, "hasil.zip", NULL};
    //     execv("/usr/bin/unzip", arg);
    // } else {
    //     while ((wait(&status)) > 0);
    // }

    FILE *f = fopen("no.txt", "wr");
    fprintf(f, "No");
    fclose(f);

    if (fork() == 0) {
        char *arg[] = {"zip", "-r", "-P", buf, "hasil.zip", "hasil/", "no.txt", NULL};
        execv("/usr/bin/zip", arg);   
    } else {
        while ((wait(&status)) > 0);
    }

    for(i=0;i<4;i++)
    {
        int err = pthread_create(&tid4[i], NULL, &useless_thing, NULL);
        if (err != 0)
            printf("\ncan't create thread :[%s]", strerror(err));
    }
    for(i=0;i<4;i++)
    {
        pthread_join(tid4[i], NULL);
    }
	exit(0);
    return 0;
}